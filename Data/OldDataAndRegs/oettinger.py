from __future__ import division
import numpy as np
import scipy as sp
import sqlite3 as sq
import pandas as pd
import statsmodels.formula.api as sm
import bokeh.plotting as bk


conn = sq.connect("newData.db")

data = pd.read_sql("SELECT incwage, age, hsless, somecoll, colldegree, gradschool, black, hispanic, married, female, nchild, wah, ind, occ, wkswork2, uhrswork FROM acscity WHERE year=1990 AND age<65 AND age>24 AND ind <900 AND wkswork2>4 AND uhrswork>34", conn)


data['weeks'] = 49
data.loc[data['wkswork2']==6,'weeks']=52
data['loghwage'] = np.log(data['incwage']/np.multiply(data['weeks'], data['uhrswork']))


modelOLS = sm.ols("np.log(incwage) ~ wah + age + I(age^2) + hsless + somecoll + colldegree + gradschool + black + hispanic + married + nchild + C(ind) + C(occ)", data = data)
#reg = modelOLS.fit()
#print reg.summary()


xname = modelOLS.exog_names
yname = modelOLS.endog_names
x = sp.sparse.csr_matrix(modelOLS.exog)

n = np.shape(x)[0]
k = np.shape(x)[1]

y = modelOLS.endog
xx = x.T.dot(x)
xy = x.T.dot(y)
betahat = sp.sparse.linalg.spsolve(xx, xy)


res = y - x.dot(betahat)
sigesq = np.dot(res.T, res)/(n-k)
xxinv = sp.sparse.linalg.spsolve(xx, np.eye(xx.shape[0],xx.shape[1]))

##### Covariance Matrix corrected for Selection Model

covar = sigesq * xxinv
varbeta = np.diag(covar)


##### Calculate T-stats and P values

tstat = betahat/np.sqrt(varbeta)

pval = sp.stats.t.pdf(tstat, n-1)


##### Store Results
#results = {"Covariates": xname, "Coef": betahat, "StdErr": varbeta, "Tstat": tstat, "Pval": pval}
results = dict()

index = 0
for i in xname:
    results[i]={"Coef": round(betahat[index],4), "StdErr": round(varbeta[index],4), "Tstat": round(tstat[index],4), "Pval": round(pval[index],4)}
    index = index + 1

print results['wah']