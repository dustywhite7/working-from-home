##### IMPORTS
import pandas as pd
import numpy as np
import scipy as sp
import statsmodels.api as sm
import os

##### READ DATA
os.chdir(os.getcwd())
os.getcwd()
data = pd.io.stata.read_stata('data.dta')
print np.shape(data)

data = data[data['hhincome']!=9999999]
data = data[data['region']!=99]
data = data[data['statefip']!=99]
data = data[data['related']!=9999]
data = data[data['fertyr']!=8]
data = data[data['school']!=9]
data = data[data['educ']!=0]
data = data[data['educd']!=999]
data = data[data['empstat']!=0]
data = data[data['vetstat']!=9]
data = data[data['vetstatd']!=99]

print np.shape(data)

data = data[data['age']>18]
data = data[data['incwage']>0]

print np.shape(data)

data.to_hdf('dataHDF.h5', 't')
