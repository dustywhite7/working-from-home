gen lnwage = log(hrwage)
reg lnwage age-nchild if lnwage>1 & lnwage<150 & wkswork2>4 & uhrswork>34 & age>24 & age<65 & pubemp==0
ivregress 2sls lnwage age-married ind1-nchild (wah = age-married ind1-nchild nchlt5) if lnwage>1 & lnwage<150 & wkswork2>4 & uhrswork>34 & age>24 & age<65 & pubemp==0
