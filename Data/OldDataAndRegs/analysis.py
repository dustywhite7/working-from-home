##### IMPORTS
import pandas as pd
import numpy as np
import scipy as sp
import statsmodels.api as sm
import os

##### READ DATA
os.chdir(os.getcwd())
os.getcwd()

# # Full data
# data = pd.read_hdf('dataHDF.h5','t')

data = pd.read_hdf('data13HDF.h5','t')

data.head()

data['wah'] = (data['tranwork']==70)
# for i in np.unique(data['statefip']):
#   varstring = 'statefip' + str(i)
#   data[varstring] = (data['statefip']==i)

# for i in np.unique(data['occ']):
#   varstring = 'occ' + str(i)
#   data[varstring] = (data['occ']==i)

# for i in np.unique(data['ind']):
#   varstring = 'ind' + str(i)
#   data[varstring] = (data['ind']==i)

# for i in np.unique(data['statefip']):
#   varstring = 'statefip' + str(i)
#   data[varstring] = (data['statefip']==i)

# for i in np.unique(data['region']):
#   varstring = 'region' + str(i)
#   data[varstring] = (data['region']==i)

data['vet'] = (data['vetstat']==2)

data['hsless'] = (data['educd']<65)
data['somecoll'] = (data['educd']>64) & (data['educd']<101)
data['colldegree'] = (data['educd']>100) & (data['educd']<114)
data['gradschool'] = (data['educd']>113) & (data['educd']<117)

data['student'] = (data['school']==2)

data['yearbaby'] = (data['fertyr']==2)

data['married'] = (data['marst']<4)

data['age2'] = data['age']^2

data['female'] = (data['sex']==2)

data['headhouse'] = (data['relate']==1)

data['multigen'] = (data['multgen']>1)

data['farmer'] = (data['farm']==2)

data['busowner'] = (data['incbus00']>0)

np.shape(data)

data.to_csv("data13.csv")


