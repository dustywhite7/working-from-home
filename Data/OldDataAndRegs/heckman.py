##### IMPORTS
import pandas as pd
import numpy as np
import scipy as sp
import statsmodels.api as sm
import os
import matplotlib.pyplot as plt
import sqlite3

##### READ DATA
os.chdir(os.getcwd())
os.getcwd()

conn = sqlite3.connect('data.db')
c = conn.cursor()

data = pd.read_sql_query('SELECT * FROM Data WHERE year=2013 AND age>17 AND empstat=1', conn)



#####                                         #####
#####     DATA ANALYSIS - WITH MANUAL OLS     #####
#####                                         #####


##### STEP 1 - Run Probit

from statsmodels.formula.api import probit
model = probit(formula = 'wah ~ female + age + I(age^2) + black + other + hispanic + hsless + somecoll + gradschool + student + yearbaby + married + busowner + farmer + multigen + headhouse + C(occ)', data = data, missing = 'drop')
results = model.fit(method = 'bfgs', maxiter = 5000)


##### Generate Lambda hat
pred = results.predict()
stdnorm = sp.stats.norm()
lamb = stdnorm.pdf(pred)/stdnorm.cdf(pred)
data['lamb'] = lamb

##### Generate delta hat
delt = lamb*(lamb + pred)
np.shape(delt)

##### Store W matrix and covariance matrix
w = model.exog
vargamma = results.cov_params()

del model, results, pred

##### STEP 2 - Run OLS Regression
from statsmodels.formula.api import ols
modelOLS = ols(formula = 'np.log(incwage) ~ lamb + wah + female + age + I(age^2) + black + other + hispanic + hsless + somecoll + gradschool + student + yearbaby + married + busowner + farmer + multigen + headhouse + C(occ) + C(statefip)', data = data, missing = 'drop')


##### Store X matrix, Y matrix, and n (samples)
x = modelOLS.exog
xname = modelOLS.exog_names
y = modelOLS.endog
yname = modelOLS.endog_names
n = np.shape(x)[0]
k = np.shape(x)[1]

countlam = 0
while xname[countlam]!='lamb':
    countlam = countlam+1

del data, modelOLS


##### Calculate sigma hat squared (of epsilon)
betahat = np.dot(np.linalg.pinv(np.dot(x.T,x)),np.dot(x.T,y))
res = y - np.dot(x, betahat)
sigesq = np.dot(res, res)/(n) + np.mean(delt)*(betahat[countlam])**2


##### Calculate rho hat squared
rhosq = (betahat[countlam]**2)/sigesq
rhosq


##### Calculate Q term

rdelt = np.ones(np.shape(delt))-rhosq*delt
Delt = sp.sparse.diags(rdelt, 0)
D = sp.sparse.diags(delt, 0)



left = np.dot(x.T, D.dot(w))
mid = np.dot(left, vargamma)
right = left.T


q = np.dot(mid, right)
q = rhosq * q

del rdelt, D, left, mid, right


##### Covariance Matrix corrected for Selection Model

xxinv = np.linalg.pinv(np.dot(x.T, x))
mid2 = np.dot(x.T, Delt.dot(x)) + q
covar = sigesq * np.dot(np.dot(xxinv, mid2), xxinv)
varbeta = abs(np.diag(covar))
##### Calculate T-stats and P values

tstat = betahat/np.sqrt(varbeta)

pval = sp.stats.t.pdf(tstat, n-1)


##### Print Results

print "-"*100
print str('Variable').ljust(20) +str('Coefficient').ljust(20) +str('Variance').ljust(20) +str('T-stat').ljust(20) +str('Pr(|t|>0').ljust(20)
print "-"*100
index = 0
for i in xname:
    print str(i).ljust(20) + str(round(betahat[index], 5)).ljust(20) + str(round(varbeta[index], 5)).ljust(20) + str(round(tstat[index], 5)).ljust(20) + str(round(pval[index], 5)).ljust(20)
    index = index + 1
print "-"*100
