##### IMPORTS
import pandas as pd
import numpy as np
import scipy as sp
import statsmodels.api as sm
import os

##### READ DATA
os.chdir(os.getcwd())
os.getcwd()
# data = pd.io.stata.read_stata('data.dta')
data = read_hdf('dataHDF.h5')

# ##### GENERATE DATA FOR SINGLE YEAR, CLEAN IT UP
# np.max(data['year'])

# data13 = data[data['year']==2013]



# np.shape(data13)

# data13.columns.values

# data13['hhincome'] = data13['hhincome'].replace(9999999, 'NaN')
# data13['region'] = data13['region'].replace(99, 'NaN')
# data13['statefip'] = data13['statefip'].replace(99, 'NaN')
# data13['related'] = data13['related'].replace(9999, 'NaN')
# data13['fertyr'] = data13['fertyr'].replace(8, 'NaN')
# data13['school'] = data13['school'].replace(9, 'NaN')
# data13['educ'] = data13['educ'].replace(0, 'NaN')
# data13['educd'] = data13['educd'].replace(999, 'NaN')
# data13['empstat'] = data13['empstat'].replace(0, 'NaN')
# data13['vetstat'] = data13['vetstat'].replace(9, 'NaN')
# data13['vetstatd'] = data13['vetstatd'].replace(99, 'NaN')



# data13 = data13.dropna(axis=0)
# np.shape(data13)


# ##### CLEAN UP FULL DATASET
# np.shape(data)

# data.columns.values

# data['hhincome'] = data['hhincome'].replace(9999999, 'NaN')
# data['region'] = data['region'].replace(99, 'NaN')
# data['statefip'] = data['statefip'].replace(99, 'NaN')
# data['related'] = data['related'].replace(9999, 'NaN')
# data['fertyr'] = data['fertyr'].replace(8, 'NaN')
# data['school'] = data['school'].replace(9, 'NaN')
# data['educ'] = data['educ'].replace(0, 'NaN')
# data['educd'] = data['educd'].replace(999, 'NaN')
# data['empstat'] = data['empstat'].replace(0, 'NaN')
# data['vetstat'] = data['vetstat'].replace(9, 'NaN')
# data['vetstatd'] = data['vetstatd'].replace(99, 'NaN')



# data = data[pd.notnull(data['hhincome'])]
# data = data[pd.notnull(data['region'])]
# data = data[pd.notnull(data['statefip'])]
# data = data[pd.notnull(data['related'])]
# data = data[pd.notnull(data['fertyr'])]
# data = data[pd.notnull(data['school'])]
# data = data[pd.notnull(data['educ'])]
# data = data[pd.notnull(data['educd'])]
# data = data[pd.notnull(data['empstat'])]
# data = data[pd.notnull(data['vetstat'])]
# data = data[pd.notnull(data['vetstatd'])]

# np.shape(data)

# data = data[data['age']>=18]

# data.to_hdf('dataHDF.h5', 't')


# pd.unique(data13['hhincome'])


# for name in range(len(data13.columns.values)):

#   print data13.columns.values[name].ljust(20) + str(np.mean(data13[data13.columns.values[name]])).ljust(20)

# data13.columns.values
# type(data13[:1])

# y = data13['incearn']
# xnames = ('year', 'hhincome', 'school','educ')
# x = pd.DataFrame()
# for i in xnames:
#   x = x.append(data13[i])
# x = x.T

# reg = sm.OLS(x,y)

# np.shape(x)
# np.shape(y)
