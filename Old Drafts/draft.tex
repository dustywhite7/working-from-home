\documentclass[10pt,letterpaper,draft]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{titling}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage[round, sort]{natbib}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}
    \vskip0.5em}%
}
\linespread{2}

\makeatletter
\renewcommand\@biblabel[1]{}
\makeatother


\title{Working at Home Pays:}
\subtitle{Modeling the decision to work from home}
\author{Dusty White\\
  \small Graduate Student
  \small Washington State University \\
  \small
  \small
}

\begin{document}


\maketitle
\thispagestyle{empty}
\abstract {This paper explores the ability of some workers to work from home, and the impact that such a decision has on the income of the worker. I model the exchange between the firm and individual as they choose a wage for the individual, and decide whether or not that individual will be permitted to work from home. The model presented suggests that working from home should be considered a form of compensation, and that workers who work from home should therefore have lower income than their counterparts who do not. By making use of the 2004 Work Schedules and Work at Home Supplement of the Current Population Survey, I find individuals who work from home earn more than those who do not due to productivity differences. This finding suggests that only the most productive workers are permitted to work from home.}
\\ \\ \\ \\
\noindent
\small Keywords: work from home, compensating differential\\
\small JEL Classification: J31, J33, J22 \\

\pagebreak

\setcounter{page}{1}
\section{Introduction}

Historically, it has been impractical to work from home because work needed to be done in fields, in factories, or in offices where clients could find and contact the necessary skilled workers. With the advent of modern telecommunications and computing, it has become possible for workers in many industries to do some, if not all, of their work from home. A physician can enter notes on a laptop, a programmer can write and execute code from a personal computer, clients can contact lawyers, architects, and consultants on their cellular phones or through video communication on those same phones or other mobile devices.\par
        These leaps in technology imply that the losses in productivity of working from home may no longer be as large as they once were. This possibility is supported by recent research by Gerald Oettinger~\citep{oettinger2011incidence}, which suggests that the productivity cost of working from home fell by 30 percent between 1980 and 2000, and is falling fastest in industries which have experienced the greatest advances in Information Technology. Oettinger's results show that employment from home has risen at a much larger rate than the workforce as a whole during that same time period. This increased opportunity for work from home makes understanding the decision made by firms and employees of whether or not to work from home even more important than ever.\par
        As the cost in productivity of working from home decreases, maintaining the traditional office infrastructure becomes relatively more expensive. Some firms are capable of shifting work out of the office and into work from home without suffering large drops in productivity, while other firms require work that can be more efficiently performed in the traditional office setting. Because of this, I do not expect the relationship between working from home and wage to be identical across occupations.\par 
       Working from home has the power (at least conceptually) to increase the welfare of both firms and employees. For the firm whose employees do not suffer large drops in productivity at home, and whose work can be done from one location as well as another, there exists a potential to decrease the fixed costs of producing by reducing the amount of office infrastructure (office building, computers, Internet connection, supplies, etc.) and allowing employees to work from home. Additionally, since working from home is considered a desirable work trait to at least a fraction of the workforce, those workers who would prefer to work from home might be willing to accept a lower wage to work from home, thus avoiding the inconvenience of a long commute, or any other aspect of traveling to work that decreases their utility. Thus, the firm who allows workers to work from home stands to gain from the decreased fixed cost of production, as well as the potential to reduce wages to employees who are willing to accept a lower wage in order to be able to work from home. \par
        This paper seeks to explain why firms allow some workers and not others to work from home. I seek to understand whether workers who choose to work from home are more effective, and whether or not firms experience lower costs for allowing workers to work from home.\par
Sherwin Rosen (1986) credits the idea of a compensating differential to Adam Smith (1776), who in his Wealth of Nations suggests that workers who encounter more miserable working conditions will receive higher wages in order to induce them to accept those working conditions~\citep{rosen1986theory}. In recent decades, this concept has been applied to various aspects of the workplace environment, from dangerous conditions~\citep{rosen1986theory}, to the differential inspired by varying commute time~\citep{paul1986compensating}. Because of the many and varied applications of compensating differentials, it is reasonable to think of most variation in wage as being due to some sort of equalizing difference for a condition inherent in the job of interest. For example, increased wage due to experience can be thought of as the compensating differential for not retiring to spend more time with family. Higher salaries for college graduates can be thought of as compensating differential for subjecting oneself to the rigor of the college education until completion of a degree. The differential of interest in this paper, however, is the compensating differential experienced by workers who choose to work at home. \par
The literature on the choice made by employers and employees who choose to work at home is sparse. While some articles summarize the subsets of the workforce who are most likely to enjoy the option of working from home~\citep{mccrate2005flexible}, they focus mostly on the lack of equal opportunity for workplace flexibility. Working from home is more likely to be an option for white male workers in managerial roles, all else equal ~\citep{golden2009flexible, golden2008limited}. While this gives insight into unequal working conditions for individuals of varying background, the research on trends does not explain why any subset of workers has the ability to choose to work from home, nor does it explain how they make that decision. On the closely related subject of flexible work hours, there are a few simple models demonstrating the effect of compensating differentials on the wages of workers with flexible schedules, and suggesting that the compensating differential is in fact relatively small in this case ~\citep{johnson1996relationship, mccrate2005flexible}.While I believe that a compensating differential does exist for those who choose to work from home, I think that the effect of working from home on firms might be equally important in the joint decision between firm and employee.\par
Recently, there has been research suggesting that the number of individuals working from home has increased more quickly than the number of individuals working in traditional on-site jobs ~\citep{oettinger2004growth}. Oettinger also finds that recent advances in Information Technology have led to reductions in the compensating difference caused by working from home. In his 2010 paper, Oettinger finds that the industries that experienced the most rapid increase in working from home were also the industries in which “on-the-job IT” was found to increase by the largest amount~\citep{oettinger2011incidence}. Research demonstrating the rapid increase in the number of workers choosing to work from their home instead of the office, and that the compensating differential for doing so is decreasing, suggests that the decision to allow employees to work from home will become even more important as more firms seek to understand the costs and benefits of allowing work from home.\par
While Oettinger does explore the compensating differential experienced by wage and salary workers who choose to work from home, in no article to date has a model been suggested that explains why certain workers are allowed to flex their schedules, nor why some have the option of working from home and some do not. Certain industries such as manufacturing and construction would not fit any model of working from home, since the jobs imply a task that must be done at a given location, while others such as software development, journalism, or academic research could reasonably be performed from outside of the office environment. This paper attempts to explain why firms whose workers could plausibly be productive from home offer the option of working from home, as well as how the workers at these firms choose to work at the office or from home. \par
	The model is a simple equilibrium model in which consumers choose consumption and labor in order to maximize utility, and firms choose how much work is done from home in order to maximize their profits. The model suggests that wage should be higher where less work is done from home, and also should be higher where productivity at home is higher. These testable results are the basis for the empirics that follow.
    The results of my analysis are surprising, and contrary to the model presented. If there is a differential offered to workers for working from home, it is in fact an increase in salary, and not a decrease. I postulate that this increase in salary is not caused by the choice to work from home, but is instead stems from the most productive employees at a given firm being the most likely to work from home. It thus stands to reason that instead of workers seeking to work from home and accepting a pay cut, the most productive workers are offered the opportunity to work from home as increased incentive to remain loyal to a given firm, instead of moving on to a new position with a slightly higher wage at a new firm.\par
	My contribution is to build on the work done by Oettinger and others in estimating the compensating differential associated with working from home through the creation a simple model that explains the underlying decision between firm and employee of whether or not an employee will work from home. This model demonstrates that the decision to work from home benefits not only workers, but also the employer through reduced cost of production. Additionally, I provide evidence from the CPS and Economic Census to support the model. \par
	The rest of the paper is organized as follows. I first present a model that attempts to explain the decisions made by firm and employee as they negotiate wage and the opportunity to work from home. In this section I also discuss the consequences of the model and what it implies about wage, firm's costs, and working from home. In the next section, I discuss the characteristics of the data that will be used to perform an empirical analysis based on the model from the previous section. I then proceed to explain and analyze the results of the empirical model, and conclude by explaining the relationship between the theoretical and empirical models.\par

\section{Model}
	***This model is still under development and likely contains errors***\par
	My model considers both the decision of the consumer, who is also the worker, as well as the firm for which that consumer may work. There are $i=1,...,I$ consumers and $j=1,...,J$ industries, where $I$ and $J$ are chosen exogenously. A consumer is a utility maximizer who first draws from an exogenous distribution the industry in which he will work. Having been selected into an industry, the individual then faces the following maximization problem, in which he chooses $C$ and $L$ (consumption and amount of labor provided, respectively) to maximize utility:
	\begin{equation}
	max \;\; U_i = \omega \;log\;c+(1-\omega)\;log\;\big( \bar{L} -L + l(x_j) \big)\;\;\;s.t.
	\end{equation}
	\begin{equation}
	c = \hat{w} L,\;\; 0\leq L\leq \bar{L} ,\;\;c\geq 0,\;\;\bar{L} = 1
	\end{equation}
	where $\bar{L}$ is the amount of labor a worker is endowed with, $\omega$ is the weight given to consumption in the utility function, and $l(x_j)$ is the utility returned to the consumer when working from home. This value could be considered the ability to avoid long commutes, or to perform minor home-based tasks during the workday.\par
	The firm representing industry $j$ is assigned (again exogenously) a proctivity value $\big( x_{j} \big)$ for working from home, between 0 and 1.Employees of firm $j$ who work in the office (or any other "official" workplace) have productivity 1, independent of industry. Those who work from home have productivity $x_j$. The firm thus has technology
	\begin{equation}
	y_{j} = \big( \nu \cdot l(1)^{\frac{s-1}{s}}+(1-\nu)\cdot l(x_j)^{\frac{s-1}{s}} \big) ^{\frac{s}{s-1}}
	\end{equation}
where $y_j$ is the output of firm $j$, $\nu$ is the fraction of work performed in the workplace, and $s$ is the elasticity of production. This results in the following profit function
	\begin{equation}
	\pi_{j} = y_{j} - \hat{w}L - c(\nu)
	\end{equation}
	where $pi_j$ is firm $j$'s profit, $w$ is the wage paid to workers, and $c(\nu)$ is the cost of infrastructure as a function of how much work is done from home. This cost function is increasing and convex in $\nu$. The firm thus chooses $\nu$ in order to maximize profits.\par
	For the purposes of this paper, I am interested primarily in the expected sign of the effect of working from home on wage. Therefore, I will make some extreme simplifying assumptions in order to arrive quickly at the point at which the model makes predictions that can be observed using available data. The assumptions are as follows: $\omega  = .5$ so that leisure and consumption are valued equally, $J=1$ so that there is one representative firm and one representative consumer, $s \rightarrow \infty$ such that CES production becomes linear, $l(x) = \sqrt{x}$ is the return to working from home, and $c(\nu) = \nu^2$ is the cost function dependent on $\nu$.\par
	Based on these assumptions, the representative consumer has the first order condition
\begin{equation}
0 = \frac{.5}{\hat{w}L} + \frac{.5}{\bar{L}-L+l(x_j)}
\end{equation}
suggesting that
\begin{equation}
\hat{L} = \frac{\hat{c}}{\hat{w}}=\frac{\bar{L}+l(x_j)}{1+\hat{w}}
\end{equation}
indicating that the consumer will choose to work more when they are able to work from home more often, and thereby recover a larger portion of the utility that would be lost by devoting time to labor, and that increased wage will cause the consumer to spend less time working. \par
Under the above assumptions the representative firm for has profit function 
\begin{equation}
\pi(j) = (1-\nu)\sqrt{x_j}\cdot L + \nu L - \hat{w}L - \nu^2
\end{equation}
This results in the following FOC:
\begin{equation}
0 = -L \sqrt{x_j} + L -2\nu \;\;\implies \hat{\nu}=\frac{L(1-\sqrt{x_j})}{2}
\end{equation}
showing that firms allow more work to be done from home as the productivity of working from home increases.\par
Finally, the market clearing condition brings the results of the model into focus
\begin{equation}
y(j) = c = \hat{w}L
\end{equation}
which gives the following function
\begin{equation}
\hat{w} = \sqrt{x_j} + \nu(1-\sqrt{x_j})
\end{equation}
This function is increasing in $\nu$ as well as in $x_j$, suggesting that as more work is done in the office, wage increases (or, conversely, working from home should reduce wage), and that as working from home becomes more effective, wage should also increase. These results are testable using publicly available datasets, such as the Current Population Survey. I will use the data from the following section to test whether or not the results of this model hold in reality.


\section{Data}
The Current Population Survey (CPS) is a national survey in the United States meant to create an annual cross-section of the national population. The survey tracks variables from demographics to work characteristics and health practices. The 2004 Work Schedules and Work at Home Supplement asks respondents a series of questions about the flexibility of their work schedule and whether or not they work at home. The CPS 2004 Work Schedules and Work at Home Supplement was obtained from the ICPSR website~\citep{CPS}. \par
	The survey contains 155,064 observations, with over 1500 from each state. The dependent variables in the model from this dataset are logged annual income, whether or not an individual is permitted to perform paid work at home, whether or not the individual performs any work from home, and how many days each week are spent working from home. Table (?) shows summary statistics for each of these variables. Other important included variables are level of education, age, metropolitan status, industry of employment, race and region. Also included is the square of age, in order to account for differing trends at younger and older ages of workers.\par
	The summary statistics of the data suggest that the data is a reasonable representation of the national population, with an appropriate mean wage of \$50,002 and a median of \$40,000 (the median in 2004 in the US as a whole was approximately \$44,000 according to the US Census Bureau), with 9.9\% black and 10.2\% hispanic respondents. The industries included are grouped together generally and in more detailed subcategories, as can be seen in Table (?) . While these industries are rather broad, they will be sufficient for our purposes as we seek only to understand which industries are most likely to allow their employees to work from home in order to be able to derive the effect of allowing workers to work at home on the costs of the firms in a given industry.\par
	The American Time Use Survey (ATUS) is a cross-sectional national survey in the United States focused on understanding how individuals use their time on average. Activities are logged and returned to the survey administrators, who then process the logs into statistics based on the amount of time during a given day that an individual would perform a certain activity~\citep{ATUS}.\par
	Since I have not yet cleaned the data from the ATUS, I can provide no useful information on the variables that I will be focusing on. I hope to use the survey to model the time value of commuting to work, and back out the value that individuals assign to working 0 minutes from their home (or in other words, working from home). \par
	The data of interest at the firm level is the Economic Census performed nationally in the United States across firms in most industries. Again, since I have not yet cleaned the data from the Economic Census, I can provide no useful information on the variables that I will be focusing on at this time. It is my hope that the costs of employee payroll will be lower, all else equal, in industries with high rates of working from home. I also hope to find that the costs of infrastructure (office space, etc.) will be lower in those same industries.

	
\section{Results}

The employee segment of the model will be analyzed empirically using the ICPSR version of the CPS 2004 Work at Home Supplement. The data represents a relatively small segment of the workforce, and because of this small sample size, it lacks robustness to changes in the empirical model. Table 2 results from various regressions using family income as the dependent variable, and seeking to explain the effect of working from home on income. The effect of each day during the week on which an individual works from home is an increase of \$825 in annual income. Additionally, individuals who are permitted to perform paid work from home make approximately \$1278 more than their counterparts who are not permitted to do so, contradicting the results that are predicted when considering compensating differentials as the main force behind the wage difference. \par
A negative compensating differential for the opportunity to work from home is to be expected: a desirable trait in a job should have the opposite effect of a job characteristic which makes a given job less desirable. Therefore, while workplace hazards and other undesirable work characteristics increase salary, we expect (and find) that the opportunity for an employee to work from home makes that worker willing to forego some amount of annual income in exchange for being permitted to work from home during the entire workweek. The data contradicts this theory, and in fact we find that those who are permitted to perform paid work from home in fact make more money annually than their coworkers who are not permitted to work from home.\par
In Table 1, three different regressions using various survey questions regarding work from home each indicate the same surprising result: it pays to work from home. While this is surprising in light of the law of compensating differentials, it may in fact be explained by other factors that are unobservable in the model in its current state. In fact, a better way to understand this might be to consider working from home as a reward meted out by the employer in order to increase loyalty to the firm by the most productive workers.(?) state in their paper that the option to flex schedule, and also to work from home, increases employee loyalty. Perhaps the increase in wage found in these models can be explained by the fact that only the most productive workers are permitted to work from home as a way to keep them from moving to a job that offers better amenities.\par
After revising the regression to consider working from home as the dependent variable, it is plausible that individuals with greater productivity are more likely to work from home. Since the best measure of productivity, income, has a near-zero coefficient, we resort to education as a proxy for productivity, and find that individuals who are college graduates (and even those with some college experience but no degree) are significantly more likely than high school graduates to be able to work from home. This suggests that firms are more willing to allow the most productive workers to work from home, while being less likely to grant the same privilege to less productive workers, if you accept the premise that individuals who attended college are more productive than those who did not (see Table 2).\par
	**Because the original results were not significant, and the data was hard to work with, I ended up spending a lot of time looking for a new data source. I have found new data, but have not yet had time to incorporate it into the paper. Unfortunately, this means that I do not yet have robustness measures, since I felt that finding adequate data should take precedence.**\par
	Here, I would continue on by using the American Time Use Survey (if I had a bit more time to work on this dataset and clean it up) in order to attempt to model willingness to pay for a 0 minute commute (or in other words, willingness to pay for working from home). Hopefully these results would be significant, and would allow me to proceed to the employer segment of the model.\par
For the employer segment of the model, I plan to use the Economic Census data to model the value to firms of having employees working from home. I will do so by comparing firms in industries where working from home is feasible to firms in other industries, hoping to demonstrate that the costs associated with operating a firm in a sector where employees work from home are lower than in similar industries where the same is not possible. By doing so, I will demonstrate that there is value to the firm, as well as to the employee of working from home.\par
Unfortunately, data has not been kind to me so far, so I am still working on my second dataset for employee information, as well as my dataset for the firms’ information. When these are done, and I can perform the analysis that I want to on each side of the model, I then plan on comparing the results of the data to the expected results given by my model from the theoretical model section of the paper. Hopefully, this will allow me to demonstrate that there is incentive in the marketplace for employers to offer for employees to work from home under certain conditions, and that both parties will be better off.



\section{Conclusion}
In recent years there has been an increase in the number of workers choosing to work from home, and data suggest that the rate of increase in employment from home is larger than the increase in employees hired to work in office settings in recent years. Because of this shift in the demography of labor in the United States, it is important to understand how firms and workers cooperate to determine whether or nota worker will work from home, and what the wage of that worker will be.\par
Using data from the Current Population Survey, the American Time Use Survey, and the US Economic Census, I find that if there is a compensating differential for working from home, it is outweighed in the data by the greater productivity of workers who are permitted to work from home. In other words, workers who work from home tend to make more than their office-based counterparts mainly because those who work from home are considered more productive, and therefore paid higher wages independent of whether or not they work from home. The worker who is permitted to perform paid work from home tends to make \$1278 more annually than the worker who is not permitted to do so. In addition, each day that the worker spends working from home increases income by \$825 annually.\par
Using the ATUS, we find that... (use this paragraph to discuss the findings of the ATUS version of the regression on workers wages. \par
On the industry or firm side, ... (use this paragraph to discuss the results of comparing the industries where it is relatively more common to work from home to those where it is less common to work from home, focusing on the costs of the firm in payroll and infrastructure costs). \par
When comparing the data to the model, we find that ... (compare findings in the data to the suggested outcomes of the model that will be developed)\par
Some possible related research questions are: How much do workers value their commute time? Can the compensating differential for working from home be isolated from the productivity difference? As technology continues to develop and it becomes ever more practical for workers to perform many tasks from any location they choose, the question of whether or not to allow workers to work from home will continue to become more important, both to firms seeking to maximize profits as well as individuals seeking to negotiate an appropriate wage for their specific situation.

\bibliography{Master}
\bibliographystyle{plainnat}

\section{Tables}
\linespread{1}
\begin{small}
\begin{table}
\centering
\caption{Three Regressions Examining the Effect of Working at Home on Income While Using Varying Controls for Occupation}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\begin{tabular}{l*{3}{c}}

\hline\hline
            &\multicolumn{1}{c}{1}&\multicolumn{1}{c}{2}&\multicolumn{1}{c}{3}\\
\hline
Days Worked at Home (per Week) &     -4415.8\sym{*}  &     -2613.5         &     -3975.7         \\
            &     (-2.01)         &     (-1.71)         &     (-1.85)         \\
[1em]
Paid to Work at Home&     14288.9         &      8178.1         &                     \\
            &      (0.97)         &      (0.86)         &                     \\
[1em]
Age         &    -24349.1         &    -22794.1\sym{***}&    -24812.1         \\
            &     (-1.57)         &     (-3.47)         &     (-1.60)         \\
[1em]
$Age^2$    &       543.0         &       509.1\sym{**} &       551.9         \\
            &      (1.48)         &      (3.21)         &      (1.51)         \\
[1em]
Union Membership       &     -9006.5         &     -1007.0         &     -9429.0         \\
            &     (-1.16)         &     (-0.26)         &     (-1.22)         \\
[1em]
Less than High School Diploma  &     -2735.7         &     -3734.1         &     -3213.0         \\
            &     (-0.42)         &     (-1.55)         &     (-0.49)         \\
[1em]
Some College &      2250.0         &      3789.8         &      2249.4         \\
            &      (0.58)         &      (1.82)         &      (0.58)         \\
[1em]
College Grad &     16764.9\sym{***}&     14165.6\sym{***}&     16541.1\sym{***}\\
            &      (3.57)         &      (4.42)         &      (3.53)         \\
[1em]
Married     &     -5462.0         &     -6356.9\sym{*}  &     -5408.8         \\
            &     (-1.31)         &     (-2.48)         &     (-1.30)         \\
[1em]
Lives in Large City   &     14330.8\sym{***}&     12868.6\sym{***}&     14469.8\sym{***}\\
            &      (4.93)         &      (8.48)         &      (4.98)         \\
[1em]
Student     &      2416.3         &      5134.6\sym{**} &      2162.0         \\
            &      (0.67)         &      (2.64)         &      (0.60)         \\
[1em]
Owns a Business&     29856.4\sym{***}&     22342.9\sym{***}&     29921.8\sym{***}\\
            &      (7.06)         &     (10.31)         &      (7.08)         \\
[1em]
Midwest     &     -6080.9         &     -8756.7\sym{***}&     -5900.3         \\
            &     (-1.47)         &     (-4.05)         &     (-1.43)         \\
[1em]
South       &     -6177.7         &     -7613.3\sym{***}&     -5995.2         \\
            &     (-1.47)         &     (-3.36)         &     (-1.43)         \\
[1em]
West        &     -2675.3         &     -4353.2         &     -2615.1         \\
            &     (-0.62)         &     (-1.90)         &     (-0.61)         \\
\hline
\(N\)       &         534         &        1846         &         534         \\
\hline\hline
\multicolumn{4}{l}{\footnotesize \textit{t} statistics in parentheses}\\
\multicolumn{4}{l}{\footnotesize \sym{*} \(p<0.05\), \sym{**} \(p<0.01\), \sym{***} \(p<0.001\)}\\
\end{tabular}
\end{table}

\begin{table}
\caption{The Effect on Income of Days per Week that a Worker Spends Working Exclusively from Home.}
\centering
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\begin{tabular}{l*{2}{c}}
\hline\hline
            &\multicolumn{1}{c}{1}&\multicolumn{1}{c}{2}\\
\hline
Days Exclusively worked at Home (per week)&       825.0         &                     \\
            &      (0.91)         &                     \\
[1em]
Paid to Work at Home&      1277.9         &      1786.4         \\
            &      (0.43)         &      (0.99)         \\
[1em]
Age         &       483.3\sym{*}  &       775.4\sym{***}\\
            &      (2.31)         &      (3.85)         \\
[1em]
$Age^2$        &      -4.967\sym{*}  &      -7.811\sym{***}\\
            &     (-2.07)         &     (-3.40)         \\
[1em]
Union Member      &     -1281.2         &     -2469.3\sym{*}  \\
            &     (-0.94)         &     (-2.06)         \\
[1em]
Less than High School Diploma  &      3636.4         &      3527.4         \\
            &      (1.49)         &      (1.45)         \\
[1em]
Some College &      6949.5\sym{***}&      6756.3\sym{***}\\
            &      (6.22)         &      (6.10)         \\
[1em]
College Grad &     17184.6\sym{***}&     17285.9\sym{***}\\
            &     (15.35)         &     (15.96)         \\
[1em]
Married     &     19820.4\sym{***}&     20111.2\sym{***}\\
            &     (21.68)         &     (23.17)         \\
[1em]
Lives in Large City   &     11646.5\sym{***}&     12551.0\sym{***}\\
            &     (13.85)         &     (15.90)         \\
[1em]
Owns a Business&     13237.6\sym{***}&     12105.2\sym{***}\\
            &     (10.61)         &     (10.56)         \\
[1em]
Midwest     &     -6319.8\sym{***}&     -6141.0\sym{***}\\
            &     (-5.33)         &     (-5.55)         \\
[1em]
South       &     -8101.3\sym{***}&     -8103.1\sym{***}\\
            &     (-6.90)         &     (-7.32)         \\
[1em]
West        &     -6270.0\sym{***}&     -5844.5\sym{***}\\
            &     (-5.25)         &     (-5.21)         \\
\hline
\(N\)       &        5467         &        6451         \\
\hline\hline
\multicolumn{3}{l}{\footnotesize \textit{t} statistics in parentheses}\\
\multicolumn{3}{l}{\footnotesize \sym{*} \(p<0.05\), \sym{**} \(p<0.01\), \sym{***} \(p<0.001\)}\\
\end{tabular}
\end{table}

\begin{sidewaystable}
\caption{Using wage to predict tendency to work from home (See Section Header for Column Labels)}
\centering
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\begin{tabular}{l*{6}{c}}
\hline\hline
            &\multicolumn{1}{c}{1}&\multicolumn{1}{c}{2}&\multicolumn{1}{c}{3}&\multicolumn{1}{c}{1}&\multicolumn{1}{c}{2}&\multicolumn{1}{c}{3}\\
\hline
Annual Income  & 0.000000353         &    8.48e-08         & 0.000000672\sym{***}& 0.000000338\sym{*}  & 0.000000161\sym{**} & 0.000000838\sym{***}\\
            &      (1.42)         &      (0.99)         &      (4.05)         &      (2.07)         &      (3.04)         &      (8.34)         \\
[1em]
Age       &     0.00569         &     0.00327\sym{*}  &     0.00778\sym{**} &     0.00148         &     0.00166\sym{*}  &     0.00260         \\
            &      (1.48)         &      (2.36)         &      (2.93)         &      (0.67)         &      (2.26)         &      (1.89)         \\
[1em]
$Age^2$        &  -0.0000476         &  -0.0000325\sym{*}  &  -0.0000734\sym{*}  & -0.00000155         &  -0.0000148         &  -0.0000177         \\
            &     (-1.08)         &     (-2.05)         &     (-2.42)         &     (-0.06)         &     (-1.73)         &     (-1.11)         \\
[1em]
Union Member       &      0.0164         &     -0.0381\sym{***}&       0.120\sym{***}&    -0.00342         &     -0.0275\sym{***}&      0.0395\sym{***}\\
            &      (0.66)         &     (-4.62)         &      (7.68)         &     (-0.23)         &     (-5.71)         &      (4.38)         \\
[1em]
Hourly Work     &      -0.114\sym{***}&     -0.0215\sym{***}&      -0.183\sym{***}&      -0.101\sym{***}&     -0.0241\sym{***}&      -0.162\sym{***}\\
            &     (-6.89)         &     (-3.59)         &    (-15.93)         &     (-9.11)         &     (-6.60)         &    (-23.61)         \\
[1em]
Hours Worked per Week&    -0.00210\sym{***}&    -0.00113\sym{***}&    0.000750         &   -0.000799\sym{*}  &   -0.000555\sym{***}&    0.000559\sym{*}  \\
            &     (-3.43)         &     (-5.42)         &      (1.88)         &     (-2.16)         &     (-4.65)         &      (2.49)         \\
[1em]
Less than High School  &    0.000119         &     -0.0150         &      0.0202         &      0.0196         &   -0.000923         &      0.0110         \\
            &      (0.00)         &     (-0.89)         &      (0.62)         &      (1.20)         &     (-0.16)         &      (1.04)         \\
[1em]
Some College &      0.0121         &      0.0152\sym{*}  &      0.0380\sym{**} &      0.0205         &      0.0135\sym{**} &      0.0278\sym{***}\\
            &      (0.59)         &      (1.99)         &      (2.59)         &      (1.70)         &      (3.29)         &      (3.62)         \\
[1em]
College Grad &      0.0456\sym{*}  &      0.0255\sym{***}&       0.175\sym{***}&      0.0579\sym{***}&      0.0276\sym{***}&       0.179\sym{***}\\
            &      (2.18)         &      (3.35)         &     (12.06)         &      (4.19)         &      (6.13)         &     (21.19)         \\
[1em]
Married     &    -0.00823         &      0.0113         &     0.00645         &      0.0133         &      0.0111\sym{**} &     0.00949         \\
            &     (-0.47)         &      (1.81)         &      (0.54)         &      (1.22)         &      (3.04)         &      (1.40)         \\
[1em]
Lives in Large City   &     0.00190         &     0.00437         &     -0.0139         &    -0.00110         &     0.00153         &     -0.0109         \\
            &      (0.12)         &      (0.79)         &     (-1.32)         &     (-0.11)         &      (0.47)         &     (-1.79)         \\
[1em]
Owns A Business&       0.129\sym{***}&      0.0227\sym{**} &      0.0525\sym{***}&       0.116\sym{***}&      0.0187\sym{***}&      0.0464\sym{***}\\
            &      (5.61)         &      (2.85)         &      (3.44)         &      (7.37)         &      (3.69)         &      (4.87)         \\
[1em]
Midwest     &      0.0139         &     -0.0105         &     0.00827         &     0.00893         &    -0.00302         &     0.00675         \\
            &      (0.64)         &     (-1.37)         &      (0.56)         &      (0.64)         &     (-0.66)         &      (0.78)         \\
[1em]
South       &      0.0275         &    -0.00834         &     -0.0162         &     0.00679         &    -0.00618         &     -0.0132         \\
            &      (1.27)         &     (-1.09)         &     (-1.11)         &      (0.49)         &     (-1.35)         &     (-1.53)         \\
[1em]
West        &      0.0879\sym{***}&      0.0143         &      0.0205         &      0.0469\sym{**} &      0.0120\sym{*}  &      0.0184\sym{*}  \\
            &      (4.01)         &      (1.85)         &      (1.39)         &      (3.29)         &      (2.55)         &      (2.08)         \\
\hline
\(N\)       &        5467         &        6451         &        5994         &       11735         &       12955         &       11947         \\
\hline\hline
\multicolumn{7}{l}{\footnotesize \textit{t} statistics in parentheses}\\
\multicolumn{7}{l}{\footnotesize \sym{*} \(p<0.05\), \sym{**} \(p<0.01\), \sym{***} \(p<0.001\)}\\
\end{tabular}
\end{sidewaystable}

\end{small}
Key to Table 3
\begin{enumerate}
\item Days Worked Exlusively From Home
\item Paid to Work from Home
\item Did any Work from Home
\end{enumerate}
\end{document}