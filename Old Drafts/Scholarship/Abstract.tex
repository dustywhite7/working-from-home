\documentclass[10pt,letterpaper,draft]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{titling}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage{mathrsfs}
\usepackage[round, sort]{natbib}
\usepackage{setspace}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\large#1\end{center}
    \vskip0.5em}%
}
\begin{document}
\section{Abstract}
\spacing{2}
\normalsize
While only a small proportion of the workforce chooses to work from home, an increasing number of workers are choosing to do so thanks to changes in the structure of workplaces and the evolving nature of telecommunications. The decision of whether or not to work from home is not made by just one party, but is part of a complex interaction between firms and employees, suggesting that a principal-agent model would be an excellent theoretical method of exploring the decision to work from home.\par 

One of the most popular economic theories applied to the decision to work from home is the theory of compensating differentials \citep{rosen1986}. This theory suggests that if working from home is assumed to be a desirable job characteristic (possibly by reducing commute time, allowing flexible scheduling, or accommodating family needs), then workers should be willing to accept a lower wage in compensation for an increase in the utility gained from a specific work arrangement. \cite{beauregard2011} and \cite{edwards2001, edwards2002} suggest that workers' well-being is increased through work-at-home arrangements, directly implying that a compensating differential should be expected when an employee works from home, since this directly increases the utility of the worker. \cite{oettinger2011} and \cite{mccrate2005} confirm compensating differentials for individuals who work from home.\par 

Individuals may be willing to pay for the benefit of being permitted to work from home, but there also exist potentially confounding factors to the simple compensating differential stemming from the desirability of flexible work schedules. \cite{kraut1987} finds that individuals who work from home tend to be less dependent on personal income. These individuals may be willing to decline jobs in which they do not receive flexibility, since the opportunity cost of remaining outside the labor market may be smaller. Furthermore,  \cite{oettinger2011} finds that the compensating differential weakens over time as technology has reduced the cost of monitoring work outside of the office and of communicating with employees from a distance. \cite{schroeder2005} even observe a reversal of the compensating differential, where the firm actually increases a worker's wage to generate an additional incentive to work from home. This may be reasonable if the reduced costs of communication outweigh the expenses incurred by renting office space for employees, or if workers have become more productive when permitted to work in the comforts of home as opposed to a traditional office.\par

The primary empirical models utilized in exploring the relationship of working from home and wage are the fixed cost model used by \cite{edwards2001, edwards2002}, and the human capital model employed by \cite{oettinger2011} and \cite{schroeder2005}. While \cite{edwards2001}, \cite{mccrate2002, mccrate2005} each briefly mention the potential application of agency theory, they choose not to utilize it in their efforts to explain the decision to work from home.\par 

In this paper, I develop a simple principal-agent model based on the work of \cite{ross1973economic} in order to develop a stronger intuition of what tradeoffs firms and individuals face as they negotiate whether or not the employee will be permitted to work from home. This model permits the interaction of compensating differentials, on the part of workers and firms, as well as decisions made with respect to observability of effort and optimal contracts. Making joint use of these concepts creates a model with explicit predictions regarding working from home across several dimensions. First, the model predicts the existence of a compensating differential that depends on the utility of the worker, as well as the costs of the firm, allowing for flexibility as workplaces and workers change over time. Second, the model predicts that variance in wage among individuals who choose to work from home should differ systematically from the variance in wages among their office-working counterparts, again dependent on changing workplace parameters. Finally, the model provides intuition regarding the change in wages over time for individuals working from home as technology reduces the cost of communication between individuals who do and do not work at central offices.\par 

In this paper, I will develop the described theoretical model of the decision to work from home, discuss the assumptions required and explore the theoretical predictions of the model. I will then present an empirical model based on the theory developed, and explore the predictive and explanatory ability of my model on the actual decision of working from home. I will then conclude by summarizing the findings discussed throughout the paper, and possible extensions.

\clearpage
\bibliographystyle{apalike}
\bibliography{master.bib}

\end{document}