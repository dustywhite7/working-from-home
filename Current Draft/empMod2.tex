\section{Empirical Model}
\label{empMod}

As stated in Section \ref{model}, the principal-agent model of working from home provides intuition regarding wages and wage variance for home-workers as monitoring costs change. In order to estimate the magnitude of these changes, and in order to determine whether wages and behavior of home-workers support the principal-agent model presented above, I divide the empirical analysis performed in this paper into two segments, each exploring one of the predictions of the model. I first estimate the wage differential for working from home, then estimate changes to the variance of wages for individuals working from home. The estimations of wage differentials are an extension of the work by \cite{oettinger2011}, while the estimations of wage variation are designed to test the validity of the principal-agent model, and are unique to this paper.

\subsection{Estimating the Wage Differential}
\label{olsmodel}

In estimating the time trends of mean wages for home-workers relative to office-workers, I choose to follow previous research by \cite{oettinger2011} and \cite{schroeder2005} by using a human capital model as the basis for my identification of the effect of working from home. The model can be expressed as:

\begin{equation}
\label{eqnOLS}
ln(W_{it}) = \alpha + \gamma_{t} \cdot WAH_{it} + \beta_{t} \cdot H_{it} + \delta_{t} \cdot X_{it} + \epsilon_{it}
\end{equation}

In this model, the logged wage earned by an individual in the year in which he or she is surveyed ($W_{it}$) is a function of the constant $\alpha$, the effect of an individual working from home at the time of response ($\gamma_{t}$), the effect of human capital controls $H_{it}$ such as education and experience (or of proxies such as age), denoted $\beta_{t}$, the effect of demographic variables $X_{it}$ (with the effects denoted by $\delta_{t}$), and the individual noise term $\epsilon_{it}$.

Because it is highly unlikely that human capital and demographic characteristics have constant effects on the wage of workers over time, regressions are run separately for each year in the data set. This allows the effect of being female, for example, to vary in its impact on the wage earned over the course of time, and is analogous to interacting year with all other effects in a single regression.

In order to facilitate comparison between previous results and those presented below, I prepare my least squares estimates following those from \cite{oettinger2011} wherever possible. I generate indicator variables for industry and occupation groups based on the groups chosen by \cite{oettinger2011}, using the 1990 classification of occupations and industries in all years in order to remain consistent across time. The number of observations in each industry and occupation groups are presented in Table \ref{table:groups}. I also follow \cite{oettinger2011} by restricting the sample to full-time employees working in the private sector who are not self-employed, and that earn between \$1 and \$150 per hour in calculated hourly wage (derived from total work income)\footnote{Results without an upper income limit are available upon request, but results in this paper will follow the design of \cite{oettinger2011} in order to verify those results, while also testing the principal-agent model's applicability to the problem of working from home.}. Full-time employees are those that work 35 or more hours per week and work 48 or more weeks per year. \cite{oettinger2011} uses a random 1\% subsample of the 5\% Public Use Microsample (PUMS) census data, as well as all observations of home-workers. I instead use the 1\% PUMS in my analysis during census years, and the complete ACS sample after 2000 in order to maintain similar response patterns in each year. This difference in sampling leads to slightly differing results, but the samples from the census years are chosen to make the samples as similar as possible to the American Communities Survey data that is used to estimate the effect of working from home after the year 2000. 


\subsubsection{Selection Bias of Home-Workers}

Selection bias is a serious concern in data that contains individuals who decide whether or not to work from home, and closely parallels the example given by \cite{heckman1979sample} of workers that do and do not belong to labor unions when participating in the workforce. Because individuals are not randomly selected into the office- and home-based groups, there are almost certainly unobservable traits associated with the choice to work from home that make home-workers different from their office-working peers, and that cannot be controlled for using observed variables.

Assuming that choosing to be employed is exogenous, workers only select between office and home work. All workers have observable wages. However, it is not true that we can observe the wage of each individual \emph{were they to work from home}. This is critical, since workers are \emph{not} randomly assigned to work from home.

If it were the case that all traits contributing to the change in utility experienced by an individual from changing work locations could be observed (or that the the difference in utility between the two options were explicitly declared), then there would be no selection problem, since all differences between individuals working from home and those working in the office could be accounted for. This is unlikely to be true in reality. In order to account for the potential bias arising from the selection of individuals into and out of the class of workers that choose to work from home, it is necessary to control for the unobserved characteristics that change an individual's utility due to working from home. The inclusion of the inverse Mills ratio $\lambda$ based on a first stage Probit estimation of the likelihood of working from home corrects the selection bias in the least squares coefficients.

Following the procedure of  \cite{heckman1979sample}, the first step of this analysis is to estimate the likelihood of each individual choosing to work from home using a Probit model. This estimation takes the following form:
\begin{equation}
Pr(WAH_i =1 | Z ) = \int^{{ Z'\gamma}}_{-\infty} \phi(t)\;dt = \Phi({Z'\gamma})
\end{equation}
where $W_i$ is an indicator variable for whether or not an individual worked from home, and $Z$ is the vector of covariates for the Probit model. From this preliminary estimation, the hazard rate for each individual is constructed as a new variable ($\hat{\lambda}$) to be included in the second stage of estimation.
\begin{equation}
\hat{\lambda}_i = \frac{\phi\left({Z_i'\gamma}\right)}{\Phi\left({-Z_i'\gamma}\right)}
\end{equation}

The hazard term is then used in the second-stage least squares estimation to account for the existence of selection bias in the variable of interest (whether or not an individual worked from home at the time of the survey) and allow the estimation coefficients to accurately reflect the marginal effect of the decision to work from home. The least squares regression equation can then be written as
\begin{equation}
\label{eqnHeck}
ln(W_{it}) = \alpha + \gamma_t \cdot WAH_{it} + \beta_t \cdot H_{it} + \delta_{t} \cdot X_{it} + \beta_{\lambda t} \cdot \hat{\lambda}_{it} + \epsilon_{it}
\end{equation}
where $\beta_{\lambda t}$ is the effect of the hazard ratio in addition to the terms from the OLS model from Equation \ref{eqnOLS}. 

While exclusion restrictions are not strictly required, I use the number of children under the age of 5 (while also controlling for the total number of children in the household) as an exclusion restriction in the first stage of the estimation. This instrument has both strengths and weaknesses. It is not immediately apparent that having young children (as opposed to older children) will change productivity when controlling for other factors such as age and education. It is reasonable, however, to suggest that the individual with young children may value being home more than someone with children who are already of school age. An F-test of the strength of the number of children under the age of 5 indicates that this is a sufficiently strong instrument. In order to consider the possible difference in strength of my instrument across genders, I provide results for males and females separately, as well as results for the working population as a whole.\footnote{Because the exclusion restriction is not strictly required for identification, selection model specifications were also run without using the exclusion restriction in the first stage. The effect of working from home on wage was identical under both specifications.}    \par 

While the number of children under the age of 5 is only included in $Z$, all other covariates are included in both $X$ and $Z$. These include demographic characteristics such as gender, race, hispanic origin, education and age, as well as characteristics such as whether or not the respondent is the head of the household, is disabled, and the total number of children reported to be living in the household (independent of age). Additionally, included in both are binary variables for industry and occupation groups. In order to obtain accurate standard error measures under the assumptions of the selection model, I calculate Heckman corrected variance estimates for the second stage estimates following \cite{heckman1979sample}.\par 

%It is not clear \emph{a priori} which of two choices is being made by an individual considering working from home. Workers may choose between working in the office and working from home, according to the wages offered based on their choice. Workers may also make the choice to either work from home or remain outside of the workforce if their reservation cost represents the opportunity cost of working. Because there are likely individuals facing each of these decisions, I present results from selection models where the selection variable is working from home as well as where the selection variable is status as a full-time employee.

%\subsubsection{Endogeneity of Working From Home}
%I assume that workers choose, exogenous to the model, to enter the workforce. Workers may choose between working in the office and working from home according to the wages offered at each location and based on their productivity, but they may also make this decision according to unobservable characteristics or preferences that may affect the ability of workers to produce equally when working from home as opposed to when working in the office. Because of this potential problem, I use an instrumental variables model in order to account for the potential endogeneity of working from home in the wage estimation equation.
%
%In order to estimate the effect of working from home on the wages of an individual worker, I must isolate the effect of working from home from other demographic effects that may also effect the wage. The reduced form equation is as follows:
%
%\begin{equation}
%\label{regression}
%log(y_i )= \alpha_1 + \beta_1 \cdot WAH_i + \beta_2 \cdot X_i + \epsilon_i
%\end{equation}
%
%Here, $y_i$ represents the income of individual $i$, $\alpha$ is a constant, $WAH_i$ is an indicator of whether or not individual $i$ worked from home, $X_i$ contains other information about the worker such as age, race, marital status, education level, family composition, and industry and occupation codes, and $\epsilon_i$ is the error term for individual $i$. It is immediately clear that information about family composition, marital status, and even the industry or occupation of an individual might have an effect not only on the wage of the individual, but also on the propensity of an individual to prefer working from home. A worker with young children or elderly family members in the household might value the ability to care for those individuals during the work day. This would not only increase the likelihood of that worker choosing to work from home if possible, but also decrease their ability to be equally productive at home as would be possible in the office.
%
%To account for the endogeneity of the choice to work from home, I first estimate the effect of all other explanatory variables on the likelihood of working from home. Additionally, I include instruments for working from home that should are correlated with the decision to work from home, but should not explain the productivity of a worker. The reduced form for the first stage estimation is therefore
%
%\begin{equation}
%\label{stage1}
%\widehat{WAH_i} = \alpha_2 + \gamma_1 \cdot Z_i + \gamma_2 \cdot X_i + u_i
%\end{equation}
%
%where $Z_i$ are the instruments, and $u_i$ is the error term for the first stage regression. This leads to a second stage estimation using Equation \ref{regression} and the results from Equation \ref{stage1} to control for the endogenous effect of working from home on wages of employees. 
%
%After accounting for endogeneity in the decision to work from home as well as controlling for observable factors affecting the wages paid to an individual (through their effect on productivity), the effect of working from home on wages can be appropriately identified. The results of this estimation will be presented and explored in Section \ref{results}.



\subsection{Estimating the Variance of Wages}
\label{sub:varest}
While the change in wage differential for working from home is integral in understanding changes to the nature of working from home over time, the principal-agent model specifically predicts that there will be differences in variance between wages of individuals working from home and individuals working in a traditional office setting where monitoring costs are greater than zero. This procedure follows work by \cite{sorensen2000equilibrium} on the dispersion of pharmaceuticals pricing.

In order to examine differences in variance, I cluster workers across five dimensions: industry, occupation, gender, year, and place of work (home or office). For each group, I generate mean values of income, race, hispanic origin, education level, student status, age, status as a business owner, whether or not the individual is a farmer, and status as head of house.\footnote{Means for race and ethnicity variables simply indicate the probability of an individual in a particular cluster belonging to a specific race or ethnicity.} Additionally, I control for the number of observations in each cluster, since variance estimates will be sensitive to the number of observations in a given grouping.\footnote{The results are robust to exclusion of the number of observations in each cluster as a variable.} Finally, I calculate the variance of logged wages for each cluster to be used as the dependent variable in estimating the effect of working from home on wage volatility.\par 

After controlling for the industry, occupation, gender and year of each cluster on the variance of wages within that subset of workers as well as the average demographics of the workers that fall in each cluster, the variance of wages remaining in each group should identify the effect of working from home on the variance of worker wages. These models are estimated using ordinary least squares.