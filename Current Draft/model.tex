\section{Theoretical Model}
\label{model}

Assume an industry in which workers face the choice of working either from home or in the office. The firm chooses wages paid to labor inputs based on the profits of the firm, and in which the firm does not experience stochastic profit levels, but the firm has an imperfect ability to monitor employee productivity (and therefore contribution to firm profits) outside the office. As the effort of an employee rises, the expected value of observed productivity ($P$ in the following equations) also increases. Suppose additionally that there are only two discrete levels of effort: high and low. There then exists a joint probability distribution of the firm's perception of employee impact on profit and true worker effort (or productivity) for a given location, which I denote as $f (P, E)$, where $P \in \{L_H,L_L\}$ represents the firm's perception of worker productivity as high or low, respectively, and where $E \in \{E_H,E_L\}$ represents high and low effort on the worker's part. I assume that office work can be perfectly monitored, whereas work from outside of the office (which I call working from home) can only be imperfectly monitored. \par 

When employees work in the office, the firm has perfect knowledge of the productivity of the employee, and therefore offers to pay a constant wage. The wage offered for home workers is minimized subject to relevant constraints in order to induce high productivity. Because this model focuses on the ability of the firm to observe worker effort, the constraints that bind the firm's choice of wage are the participation and incentive compatibility constraints common to principal-agent models. If insufficient compensation is offered to workers, they may choose not to accept a contract, or to provide a less than optimal level of effort to the firm. Because these outcomes would then result in decreased profits for the firm, the firm prefers that workers are highly productive. The following system of equations expresses the firms choice:
\begin{align}
min_{w(P)}\;\; & \int w(P)\cdot f(P\big| E_H)\; d P\;\;\;\;s.t. \\ \notag
& \int v(w(P),E, L) f(P\big| E_H)\; d P \geq \bar{w},\\ \notag
& \int v(w(P),E,L) f(P\big| E_H)\; d P \geq \int v(w(P),E) f(P\big| E_L)\; d P.
\end{align}

Wages are therefore minimized (and profits maximized) subject to the worker's expected utility based on location, wage and the (dis)utility from providing a high level of effort exceeding the worker's reservation wage ($\bar{w}$). Additionally, net expected utility of high effort conditional on location and wage must be more valuable to the worker than the net expected utility of low effort. The first order condition with respect to $w(P)$ can be expressed as
\begin{equation}
0 = -f(P\big| E_H) + \lambda v'(w(P), E, L)f(P\big| E_H) + \mu v'(w(P),E, L)\left(f(P\big| E_H)-f(P\big| E_L)\right)
\end{equation}
or
\begin{equation}
1 = \lambda v'(w(P),E,L) + \mu v'(w(P),E, L)\left(1-\frac{f(P\big| E_L)}{f(P\big| E_H)}\right)
\end{equation}
or finally as
\begin{equation}
\label{pawage}
\frac{1}{v'(w(P),E, L)} = \lambda + \mu\left[1-\frac{f(P\big| E_L)}{f(P\big| E_H)}\right].
\end{equation}
Without any other insight into the problem, this equation does not provide a meaningful understanding of the wage effect of working from home. Fortunately, making use of the principal-agent problem in the case of perfectly observable effort will increase understanding of this solution by providing a simpler solution as reference. As can be readily shown, with perfectly observable effort the solution above simplifies to the following equation:
\begin{equation}
\frac{1}{v'(w(P),E, L)} = \lambda.
\end{equation}
Workers whose effort is perfectly observable should have constant wages. This allows separate estimation of the terms $\lambda$ and $ \mu\left[1-\frac{f(P | E_L)}{f(P | E_H)}\right]$, dividing equation \ref{pawage} into two different elements which can be easily understood on their own. First, $\lambda$ is a constant that measures the wage that would be paid to a worker if she worked in the office under the assumption that effort in the office is perfectly observable. The second term, $ \mu\left[1-\frac{f(P | E_L)}{f(P | E_H)}\right]$, will dictate the wage differences of workers who instead work from home. Additionally, if work could be perfectly monitored when employees choose to work from home and costs are identical for firms in either case, then this equation indicates that the only pay difference for home-workers would be equal to the change in utility experienced by a worker when working from home as opposed to the office. Potential differences in pay would result purely from a compensating differential effect. Because it is also costly for firms to monitor the effort of workers outside of the office, base pay should differ between home- and office-workers wherever perfect monitoring is not feasible. \par 

\subsection{Changes in Monitoring Costs}
With the above information, I can now estimate the wage impact of working from home by arranging equation \ref{pawage} as follows:
\begin{equation}
v'(w(P),E,L) - \frac{1}{\lambda}= \frac{1}{\mu}\left[1-R\right]
\end{equation}
where $R$ represents the likelihood ratio $\frac{f(P | E_L)}{f(P | E_H)}$. This arrangement provides intuition about several aspects of the decision to work from home. First and foremost it aids in determining whether there exists a wage premium or a wage penalty for working from home. The equation above predicts that firms should offer individuals who work from home a greater proportion of their salary as performance-based pay relative to their counterparts who work in the office (where work is more observable). \par 

Additional insight, and testable hypotheses, can be obtained by assuming that workers face Cobb-Douglas utility functions of the following form:
\begin{equation}
v(w(P),E, L) = Lw(P)^\alpha F^{1-\alpha}
\end{equation}
where $F=1-E$, so that $F$ represents leisure (``\underline{F}un").  In order for this functional form to be reasonable, I assume that increases in $F$ represent increases in leisure, thereby increasing the utility of the individual. This simple assumption makes it straightforward to derive an effort function that will explain several effects on wages of individuals who work from home. \par 

The derivative of the utility function with respect to wage, $w(P)$, is
\begin{align}
v'(w(P),F, L)  & = \alpha L w(P)^{\alpha-1} F^{1-\alpha} \\
& = \alpha L \left( \frac{w(P)}{F} \right) ^{\alpha-1}
\end{align}
where $L$ represents a multiplier to utility that is based on the individual's gain in utility for working from home (I assume that utility is higher when working from home). This equation can then be combined with equation \ref{pawage} and rearranged as
\begin{equation}
\label{effort}
F = w(P) \left( \alpha L \left[ \lambda + \mu \left(1-R \right) \right ] \right)^{\frac{1}{1-\alpha}}
\end{equation}
in order to provide an effort function. This function represents a worker's behavior given various changes in the work environment, and changes in the ability of the firm to observe her effort.\par

Because individuals maximize utility \emph{ex ante}, the utility derived from wages may be more clearly expressed as utility of wages based on the expected value of perceived productivity, and denoted $w(E(P))$. Because wages are a function of perceived productivity (and therefore of effort), this can also be expressed as follows:
\begin{equation}
w(P) = w(\int P \cdot f(P\big| E)\; d P)
\end{equation}
The wage is then an endogenous variable in the effort function when exploring the worker's effort choice, since wages will vary as effort changes. Fortunately, assuming that increased effort leads only to a positive mean shift, and the distribution of perceived effort is otherwise unaffected, this endogeneity will not affect the sign of the derivatives presented below, but may impact the magnitude of the effect. If a change is predicted to increase effort, then it will do so both through its own effect as well as by placing upward pressure on the expected wage outcomes.\par 

 In order to determine a worker's reactions to changes in the ability of the firm to monitor effort, the derivative of the effort function with respect to the monitoring costs of the firm must be signed. The derivative of equation (\ref{effort}) with respect to $\mu$ will provide intuition about changes in the amount of time that a worker spends being productive during the day. $\mu$ represents the cost of monitoring the effort of workers. Where $\mu$ is $0$, effort is perfectly observable, and observed productivity and worker effort are equivalent. As $\mu$ increases, the cost to a firm of determining a worker's precise level of effort increases. Recall that an increase in $F$ is considered an increase in leisure. The derivative is expressed as
\begin{equation}
\frac{\partial F}{\partial \mu} = w(P) \left( \frac{\alpha L}{1-\alpha} \right) \left( 1-R \right) \left( \alpha L \left[ \lambda + \mu \left(1-R \right) \right ] \right)^{\frac{\alpha}{1-\alpha}}
\end{equation} 
Because utility is monotonic in wages, the term $\lambda + \mu \left(1-R \right)$ must always be positive, though $1-R$ need not be greater than zero. When $1-R<0$, or when a given productivity level is more likely to be perceived by the firm under low effort outcomes, then as monitoring costs increase $F$ will decrease (the worker will choose less leisure). When $1-R>0$, and a certain productivity level is more likely to be perceived by the firm under high effort outcomes, then the worker will choose more leisure as monitoring costs increase. This suggests that any increase in the cost of monitoring (expressed as an increase in $\mu$) will lead to greater fluctuation in wages among home-workers than in their office-working peers. As $\mu$ approaches $0$, behavior of home-workers should converge to the behavior of office-workers. \par

In summary, the principal-agent model provides several testable outcomes of its validity in modeling the decision to work from home. First, as monitoring costs fall, wages should converge for home- and office-workers if it is costless for firms to allow individuals to work from home. Second, as monitoring costs fall, the variance of wages for home-workers should decline relative to the variance of wages for office-workers. Since it is reasonable to assume that monitoring costs have fallen over time as communication technology has improved, wages for home-workers should rise to approximately the same level as office workers, and the variability of those wages should decline over the same time.s

%\subsection{Changes in Wage}
%Previously, I held $w(P)$ constant in order to analyze the effect of monitoring costs on worker behavior. It is also important, however, to explore the effect of perceived changes in productivity on wages for employees working from home. In order to consider this question, I add an additional assumption to the model. I assume that the distribution of perceived productivity of workers is normally distributed and has equal variance under high and low effort, so that
%\begin{align*}
%f(P\big| E_H) &\sim \mathcal{N}(b \mu_P, \sigma^2), \;b>1 \\
%f(P\big| E_L) &\sim \mathcal{N}(\mu_P, \sigma^2) 
%\end{align*}
%Using this functional form to expand the term $R = \frac{f(P | E_L)}{f(P | E_H)}$,
%\begin{align}
%R &= \frac{\frac{1}{\sqrt{2\pi \sigma^2}}\cdot exp\{-\frac{(P-\mu_P)^2}{2\sigma^2}\}}{\frac{1}{\sqrt{2\pi \sigma^2}}\cdot exp\{-\frac{(P-b \mu_P)^2}{2 \sigma^2}\}} \\
% &= exp\{\frac{(P-b \mu_P)^2}{2 \sigma^2}-\frac{(P-\mu_P)^2}{2\sigma^2}\}
%\end{align}
%Recalling Equation \ref{effort}, it is straightforward to solve for $w(P)$:
%\begin{equation}
%w(P) = F \left( \alpha L \left[ \lambda + \mu \left(1-R \right) \right ] \right)^{\frac{1}{\alpha-1}}
%\end{equation}
%Taking the derivative of $w(P)$ leads to the following equation:
%\begin{equation}
%\frac{\partial w(P)}{\partial P} = \frac{1}{\alpha-1} \left( \alpha \mu L \frac{(1-b)\mu_P}{\sigma^2} \right) F \left( \alpha L \left[ \lambda + \mu \left(1-R \right) \right ] \right)^{\frac{2-\alpha}{\alpha-1}}
%\end{equation}
%Under the above distributional assumptions, $\frac{\partial w(P)}{\partial P} > 0$ so that wage increases as the perceived productivity of the worker increases. This suggests that workers will exert lower effort where effort is less correlated with perceived productivity on the part of the firm, since wages increase in \emph{perceived} productivity and not necessarily in actual effort of the worker due to the stochastic nature of a firm's perception of productivity. Thus, home-workers in jobs (or years) where monitoring costs are high should be expected to experience greater wage penalties than home-workers in situations where monitoring costs are small.