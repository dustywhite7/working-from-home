\section{Applying Agency Theory to the Problem}
\label{model}

%In this model, I assume that workers face Cobb-Douglas utility functions of the following form:
%\begin{equation}
%v(w(Y),E, L) = Lw(Y)^\alpha F^{1-\alpha}
%\end{equation}
%In order for this functional form to be reasonable, increases in $F$ represent increases in leisure, thereby increasing the utility of the individual, and $w(Y)$ represents the wage of the worker based on output ($Y$). Workers are endowed with two units of time at work, which can be spent on either leisure or effort, where $F=1-E$, so that $F$ represents leisure (``\underline{f}un"), while $E$ represents its converse, effort. Furthermore, $L$ represents the location at which an employee works, with $L_H$ representing working from home, and $L_O$ representing work in the office. I assume that $0<L_O<L_H$, so that workers prefer to work from home, all else equal. \par 

I begin by assuming that when an employee works in the office, the firm has perfect knowledge of the productivity of the employee, and therefore offers to pay a constant wage determined by that level of productivity. This constant wage ($\bar{w}$) is based on the expected output ($\bar{Y}$) of high-effort ($E = E_H$) workers ($\bar{Y} = \int Y \cdot f(Y\big| E_H)\; d Y$)\footnote{Note that exogenous factors may still impact output at any given level of productivity.}. Because the firm can perfectly monitor office-workers, it can differentiate effort ($E \in \{E_H,E_L\}$, denoting high and low effort, respectively) from the stochastic output based on that effort ($f(Y\big| E_H)$), and offer a fixed wage to workers that supply high effort. Furthermore, assume that location can affect productivity independent of effort level, such that on average, office-workers may be more productive than home-workers, and vice versa.

\subsection{Inducing High Effort}
\label{model:high}

The wage, $w(Y)$, contingent on an output level of $Y$ being observed is offered to home workers. This wage must necessarily account for the cost to the firm of monitoring worker effort. Workers that cannot be perfectly monitored are subject to payment based on a textbook principal-agent model (see for example \cite{mas1995microeconomic}).\footnote{Treatment of the principal-agent model in Mas-Collel, Whinston and Green can be found beginning on page 478. The model in this paper differs in notation to some extent, but provides identical conclusions in all major points of the model.} This model focuses on the cost to the firm of observing worker effort. The constraints that bind the firm's choice of wage are the participation and incentive compatibility constraints common to principal-agent models, as well as a participation constraint for the firm, since the firm should not be willing to pay a worker more to work from home (net any reductions in firm cost from doing so) than would be paid to an office-worker, all else equal.

The participation constraint requires that the worker's utility be higher when engaging in work for the firm under the terms of the contract (I assume here that the contract mandates high effort) than their reservation utility (in this case, working in the office). Employment is assumed to be exogenous, but employees are given the endogenous choice to work in the office or from home. If the employee does not gain utility by working from home, they simply accept a contract at their reservation wage $w(\bar{Y})$ to work in the office, per the participation constraint. In other words, the firm must be able to pay a wage that will convince the worker that they are better off employed by the firm \emph{from home}. This will not induce all workers to choose to work from home, since some workers will not derive increased utility from working from home, and so will not accept a contract to work from home when they could instead choose to work in the office.

The incentive compatibility constraint focuses on the problem of monitoring worker effort. In order for this constraint to hold, the firm must be able to condition pay on outcomes in such a way that the expected utility from high effort (more pay, but less enjoyment from shirking) is greater than the utility from low effort (less pay, more shirking). As monitoring costs rise, and the firm has less ability to separate effort from random shocks to output, the payment for high effort must increase to induce workers into more costly signals of high effort.

The firm's participation constraint restricts the contract to paying no more in expected wage to home-workers than to office-workers, net any cost reductions (or increases), denoted $C$. This constant contains elements such as productivity differences between home and office workers, as well as the differing costs of renting office space or providing internet connections and computers to employees across the two groups. If this constraint did not hold, then the firm would simply mandate that all employees work in the office. This restriction is critical in helping to model the changes over time in wage differential between office-workers and home-workers, as can be seen below.

If an insufficient wage is offered to workers, they may choose not to accept a home contract, or to provide a less than optimal level of effort to the firm. Because these outcomes would then result in decreased profits for the firm (otherwise they would not offer a contract mandating high effort), the firm prefers that workers are highly productive. The following system of equations models the firm's choice as described above:
\begin{align}
\label{PA}
min_{w(Y)}\;\; & \int w(Y)\cdot f(Y\big| E_H)\; d Y\;\;\;\;s.t. \\ \notag
& \int v(w(Y),E_H, L_H) f(Y\big| E_H)\; dY \geq v(w(\bar{Y}),E_H, L_O),\\ \notag
& \int v(w(Y),E_H,L_H) f(Y\big| E_H)\; d Y \geq \int v(w(Y),E_L,L_H) f(Y\big| E_L)\; d Y,\\ \notag
& \int w(Y) f(Y\big| E_H)\; d Y + C \leq w(\bar{Y})
\end{align}
where $w(Y)$ is the wage paid to the employee, $E \in \{ E_H, E_L \}$ denote high and low effort, respectively, $L \in \{ L_H, L_O\}$ denote home and office locations for employment, and $v(\cdot)$ represents a utility function for employees. Output ($Y$) has a stochastic element for home-workers (just as it does for office-workers), and the distribution of output given high effort is denoted $f(Y|E_H)$. The distribution of output dependent on effort is such that the ratio $\frac{f(Y\big| E_L)}{f(Y\big| E_H)}$ decreases in $Y$ (low output is more likely at lower effort levels, and high output is always more likely at high effort levels). The expected value of $Y$ is therefore greater under high effort than under low effort ($\int Y \cdot f(Y\big| E_H)\; d Y>\int Y\cdot f(Y\big| E_L)\; d Y$). The Lagrangian of this system of equations is expressed as 

\begin{align}
\label{lagrange}
\mathcal{L} &= \int w(Y)\cdot f(Y\big| E_H)\; d Y \\
&+  \lambda \left(\int v(w(Y),E_H, L_H) f(Y\big| E_H)\; dY - v(w(\bar{Y}),E_H, L_O) \right) \nonumber \\
&+ \mu \left(\int v(w(Y),E_H,L_H) f(Y\big| E_H)\; d Y - \int v(w(Y),E_L,L_H) f(Y\big| E_L)\; d Y  \right) \nonumber \\
&- \eta \left(\int w(Y) f(Y\big| E_H)\; d Y +C - w(\bar{Y}) \right) \nonumber
\end{align}
where $\lambda$ is the shadow price of labor, $\mu$ is the shadow price of monitoring worker effort, and $\eta$ is the productivity cost of permitting employees to work from home. The $\mu$ term in particular should be expected to change over time as technology lowers the cost of monitoring effort at home.

As mentioned above, the firm must offer wages such that the expected value of utility given high effort exceeds the worker's reservation utility, which is the utility that the worker obtains from exerting (mandatory) high effort in the office. The firm must also be able to offer wages under high effort that at minimum compensate for the disutility of providing high effort on the part of the worker when monitoring is costly for the firm. If this second condition cannot be met, then firms would offer for workers to work from home only if doing so would reduce other costs (such as office upkeep) sufficiently to justify the expected reduction in output ($\int Y \cdot f(Y\big| E_H)\; d Y - \int Y\cdot f(Y\big| E_L)\; d Y$). The differences between high and low effort contracts will be described in more detail in Section \ref{model:low}.

The first order condition of the principal-agent problem from Equations \ref{PA} and \ref{lagrange} with respect to $w(Y)$ can be expressed as
\begin{equation}
\label{FOC}
1+\eta = \lambda \cdot v'(w(Y),E_H, L_H) + \mu \left( v'(w(Y),E_H, L_H)- v'\left( w(Y),E_L,L_H \right) \cdot \frac{f(Y\big| E_L)}{f(Y\big| E_H)} \right)
\end{equation}
If utility is quasilinear\footnote{According to \cite{maskin1990principal}, this assumption does not change the outcomes from the model, and is a common assumption made in applications of agency theory.} such that the utility generated by exerting low effort is additively separable from the utility generated by income or location, then $v'(w(Y),E_H, L_H) = v'(w(Y),E_L, L_H)$, and Equation \ref{FOC} simplifies to
\begin{equation}
\label{util}
\frac{1+ \eta}{v'(w(Y),E_H, L_H)} = \lambda + \mu \left(1- \frac{f(Y\big| E_L)}{f(Y\big| E_H)} \right) 
\end{equation}
The marginal utility of the worker relative to wage can now be considered as two separate elements. First, the constant $\lambda$ represents the base productivity of a worker. This value does not change for a given worker (since the model assumes that productivity does not change, but that random events may affect the resulting output). 

Second, $\mu \left( 1- \frac{f(Y\big| E_L)}{f(Y\big| E_H)} \right)$ represents the marginal utility effect to the worker of being imperfectly monitored. It expresses the cost to the worker of exerting a given level of effort based on the likelihood that the resulting productivity is a successful signal to the firm of high effort. If it becomes difficult for the firm to monitor effort, then $\mu$ increases, and the effect is amplified. If firms can perfectly monitor effort, then $\mu$ falls to zero (there is no cost to determining the effort exerted by a worker) and this term has no effect on the marginal utility of the worker. This leads to intuitive results that can be best expressed by solving the above conditions for a wage function, and examining its derivative with respect to $\mu$ (the shadow price of monitoring worker effort).

Finally, the term $1+\eta$ binds the firm to offering only those wages that make the firm at least as well off as if it only employed office-workers (firms could always make this choice, and so it is the opportunity cost of allowing work to be done from home). Thus, $\eta>-1$ is necessary for the marginal utility of the firm to be positive. This restriction simply requires that the cost of productivity loss due to working from home not be greater than the wage differential for working from home.

%Insert the wage equation and its explanation here
\subsection{Monitoring and Wage Variance}
\label{model:wage}

Recall that the outcome of the principal-agent model can be expressed as Equation \ref{util}. The first change that results from an improvement in technology is that firms are better able to monitor the effort of employees outside of the office. Assuming that $v'(w(Y),E_H, L_H)>0$ and $v''(w(Y),E_H, L_H)<0$ with respect to wage, implicitly differentiating Equation \ref{util} with respect to changes in $\mu$ results in the following equality:

\begin{equation}
\label{wagefn}
\frac{-1}{v'(w(Y),E_H, L_H)^2} \cdot v''(w(Y),E_H, L_H) \cdot \frac{\partial w(Y)}{\partial \mu} = \left(1+\eta\right)^{-1} \left( 1- \frac{f(Y\big| E_L)}{f(Y\big| E_H)} \right)
\end{equation}
The effect of monitoring costs on wage are then signed according to the sign of the right-hand side of Equation \ref{wagefn}. This is true because the sign of  $\frac{-1}{v'(w(Y),E_H, L_H)^2} \cdot v''(w(Y),E_H, L_H)$ is positive (since each element on its own is negative). 

According to the above equality, the effect of increasing monitoring costs on the wage of the employee cannot be uniformly signed. This variation in sign provides a critical insight: under the principal-agent model, a decrease in monitoring costs can be expected to have specific and predictable effects on the variance of agents' wages that are \emph{not} expected to occur under other models that predict increasing wages for home-workers over time. The sign of the derivative varies depending on whether or not the output observed by the firm was more likely to be realized under high or low effort. When a given output level is more likely to be realized under high effort, then $\frac{\partial w(Y)}{\partial \mu}$ is positive, and wages increase as monitoring costs increase. 

In the case where a given level of output is more likely to occur under low effort, then $\frac{\partial w(Y)}{\partial \mu}$ is negative, and mean wages for home-workers \emph{decrease} as monitoring costs increase. This is easily explained if firms assign a larger proportion of wages to performance-based pay instead of fixed wages as it becomes harder for the firm to directly monitor effort on the part of the worker. 

Due to the firm's participation constraint from Equation \ref{PA}, firms will not be willing to pay more (in expectation) for home-working employees to produce the same output as their office counterparts net of any changes in costs to the firm ($C$). This imposes an upper bound on how much can be paid to home-workers under high effort where monitoring is not perfect: $w(\bar{Y}) - C$. 

%As the high effort wage approaches this upper bound (and as $\mu$ continues to increase), the low effort wage must continue to fall in order to induce the worker to continue to exert high effort, even as it becomes harder for the firm to distinguish high effort from low effort. This will effectively force the mean wage for home-workers to fall as monitoring costs rise. The risk inherent in monitoring worker effort, then, must be borne by the employee in order for the firm to be willing to offer a contract for an individual to work from home.

Where the probability of the observed level of output occurring is greater under high effort than under low effort (where $f(Y\big| E_L)<f(Y\big| E_H)$), increased monitoring costs (increases in $\mu$) will increase the wages paid. Where $f(Y\big| E_L)>f(Y\big| E_H)$, wages will decrease as monitoring costs rise. This divergent payment contract leads to increases in the difference between high and low output wages, and an unambiguous increase in the variability of wages for employees, since pay is no longer constant. Where wage was constant for office-workers (who are assumed to be perfectly monitored), every increase in monitoring costs pushes wages apart for high and low output outcomes. This imposes more volatile wages on individuals working from home as monitoring costs increase, and result in decreased variability where monitoring costs fall. This prediction is unique to agency theory among explanations of wages for home-workers, and will be tested in Sections \ref{empMod} - \ref{results}.

\subsection{Shifts in the Wage Differential Due to Technology}
\label{model:wagediff}

Recall that there exists an upper bound on the amount of compensation that a firm is willing to pay individuals working from home, which is the amount that an individual would be paid to generate the same level of effort in the office, net any cost savings that might be experienced by allowing the employee to work from outside the office. Because the wage of office-workers is the \emph{upper bound} of wages for home-workers, and because home-workers may be paid less when they experience a poor draw from the output distribution (even under high effort), the average wage of home-workers is by necessity lower than the average wage of office-workers.

As technology improves and monitoring costs decline, it should be expected that wages for home-workers converge to the wages of office-workers. If there is no cost savings from allowing employees to work from home, then the wage differential will move toward zero as monitoring becomes costless. If there is a difference in cost to the firm between office- and home-work, then this difference should become the long-run wage differential between the two groups of workers.

The principal-agent model, then, makes two predictions. First, as monitoring costs fall over time, rising mean wages should be observed over the time in which monitoring costs fall. Second, the variance of wages for home-workers will be larger when monitoring costs are high, and smaller when monitoring costs are low. If wage differentials decline over time, but there is no difference in wage variance as time passes, then it is likely that some mechanism aside from agency theory is driving that change. One example would be if technology changed in such a way that home-workers became more productive relative to their office-worker peers. In this case, no change in wage variation should be expected to accompany the closing wage gap between office- and home-workers.


%Under this model, firms will offer less guaranteed salary to home-workers than to office-workers. The portion of home-workers' wages that is withheld depends on the coefficient $\mu$, which measures the difficulty of monitoring the effort of home-workers. The firm will then provide performance-based pay conditional on the realized output of the worker.

%This wage structure suggests three implications that should be observable when comparing the wages of home-workers to their office-based peers. First, since some home-workers will (either by exerting low effort or through low output random draws) realize low output, the expected wage of home-workers should be less than that of office-workers where monitoring costs exist. Second, because a portion of the home-workers' payment is dependent on a random variable (output conditional on effort), the variance of wages for home-workers should be higher than that of office-workers, again where monitoring costs exist. Finally, if monitoring costs fall toward zero, then the expected wage and the variance of wages for home-workers should converge to those of office workers.

\subsection{If Low Effort Is Preferred}
\label{model:low}

In the case that the firm realizes higher profits when workers provide low effort ($E=E_L$) but still work from home, the firm will offer the lowest possible wage that induces participation, which is $w(\bar{Y})$. If the firm cannot offer the reservation wage for low-effort work in this case (which implies that they will not offer an acceptable wage for high-effort work, since low-effort work is worth more to the firm in this case) then the firm only offers in-office employment to workers, and no employees are permitted to work from home.

If a contract can be negotiated where low-effort work is preferred by the firm, then the incentive compatibility constraint is not required, because the preferred outcome of the employee (offering low effort and receiving compensation) is also preferred by the firm. In this case, a wage is offered that can be structured as if effort were perfectly observed. Thus, an increase in overall wage might be observed if low-effort work becomes more productive over time, and the firm compensates accordingly. The wage variance (as a percentage of total wage), however, would not differ from the variance experienced by office workers. It is easy to empirically test whether or not this occurs: the low-effort case can be immediately distinguished from a case in which firms prefer high effort by whether or not changes in wage variance for home-workers can be observed as monitoring costs change.


%\subsection{An Alternative Hypothesis}
%\label{model:alt}
%
%There are potentially many different explanations for the changes in wage structure for individuals working from home increasing over time. The most likely alternative to the principal-agent model is that workers are more productive when they can network with co-workers, and modern communication has facilitated that networking from outside of the office. 
%
%The possibility that improvements in technology have made home-workers more productive through improved communication with other employees suggests that home-workers benefit more from improved networks than their office-working counterparts. This is reasonable, since office workers likely have a large group of coworkers in close proximity in both early and later years, while home-workers could conceivably go from having no close contact with coworkers in early years to close contact with many coworkers in later years. If this is the case, then home-workers would be expected to approach the productivity of their office-worker peers as their network expands. This change over time would occur as telecommunication technology improved, and would suggest a diminishing wage differential between home- and office-workers.
%
%While the networking hypothesis would successfully explain the wage differential (as would the low effort case if productivity increases for home-workers), it provides no indication that wage variance should change over time, since employees should receive fixed wages based on productivity under this hypothesis. Thus, under the networking model, the magnitude of wage variance should not change over time even as wages shift upwards. This difference in expected outcomes between the preferred principal-agent model explained above and the networking hypothesis allows me to test whether or not the principal-agent model better explains the changes in wages for home-workers over time.